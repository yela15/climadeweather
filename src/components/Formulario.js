import React, { useState } from 'react';
import Error from './Error'
import PropTypes from 'prop-types';

const Formulario = ({busqueda, guardarBusqueda, guardarConsulta}) => {

    // state del error 
    const [error, guardarError] = useState(false);

    // sxtraer ciudad y pais 
    const { ciudad, pais } = busqueda;

    // funcion que coloca los elementos en el state
    const handledChange = (e) => {
        //actualizamos el state
        guardarBusqueda({
            ...busqueda,
            // obtenemos el valor
            [e.target.name]: e.target.value
        });
    }

    // cuando el susuario da submit en el form
    const handleSubmit = e => {
        e.preventDefault();

        // validar 
        if (ciudad.trim() === '' || pais.trim() === '') {
            guardarError(true);
            return;
        }
        guardarError(false);

        // enviamos al state principal
        guardarConsulta(true);
    }

    return ( 
        <form
            onSubmit={handleSubmit}
        >
           {error ? <Error mensaje="Ambos campos son obligatorios"/> : null}
            <div className="input-field col s12">
                <input
                    type="text"
                    name="ciudad"
                    id="ciudad"
                    value={ciudad}
                    onChange={handledChange}
                />
                <label htmlFor="ciudad"> Ciudad </label>
            </div>
            <div className="input-field col s12">
                <select
                    name="pais"
                    id="pais"
                    value={pais}
                    onChange={handledChange}
                >
                    <option value="">-- Selecciona un pais --</option>
                    <option value="US">Estados Unidos</option>
                    <option value="MX">México</option>
                    <option value="AR">Argentina</option>
                    <option value="CO">Colombia</option>
                    <option value="CR">Costa Rica</option>
                    <option value="ES">España</option>
                    <option value="PE">Perú</option>
                    <option value="BO">Bolivia</option>
                </select>
                    <label htmlFor="pais"> Pais </label>
            </div>
            <div className="input-field col s12">
                <input
                    type="submit"
                    value="Buscar Clima"
                    className="waves-effect waves-light btn-large btn-block yellow accent-4"
                />
            </div>
        </form>
     );
}
 
//documentacion
Formulario.propTypes = {
    busqueda: PropTypes.object.isRequired,
    guardarBusqueda: PropTypes.func.isRequired,
    guardarConsulta: PropTypes.func.isRequired
}

export default Formulario;