import React, { Fragment, useState, useEffect } from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import Clima from './components/Clima';
import Error from './components/Error';

function App() {

  // state del formulario
  const [busqueda, guardarBusqueda] = useState({
      ciudad: '',
      pais: ''
  });

  const [consultar, guardarConsulta] = useState(false);
  const [resultado, guardarResultado] = useState({}); // aqui se almacenara el resultado de la consulta de la api
  
  // para manejar los codigos de error
  const [error, guardarError] = useState(false);

  const { ciudad, pais } = busqueda;

  useEffect(() => {
    const consultarAPI = async () => {

      if (consultar) {
        const appId = 'd10f211c94272ba9f615f249786020b5'; // recomendable consumir api key desde un servidor ( esto es vulnerable)
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;

        const respuesta = await fetch(url);
        const resultado = await respuesta.json();

        guardarResultado(resultado);
        guardarConsulta(false); // para realizar varias consultas
      }

      // detecta si ubo fallo ( ver error de codigo)
      if (resultado.cod === "404") {
        guardarError(true);
      } else {
        guardarError(false);
      }
      
    }
    
    consultarAPI();

    // eslint-disable-next-line  
  },[consultar]); //[consultar] >>> vemos los cambios que ocurren, si devuelve true se ejecuta

  // cargamos componente condicional
  let componente;
  if (error) {
    componente = <Error mensaje="No hay Resultados" />
  } else {
    componente = <Clima resultado={resultado} />
  }

  return (
    <Fragment>
      <Header
        titulo='Clima React App'
      />
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Formulario
                busqueda={busqueda}
                guardarBusqueda={guardarBusqueda}
                guardarConsulta={guardarConsulta}
              />
            </div>
            <div className="col m6 s12">
              {componente}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
